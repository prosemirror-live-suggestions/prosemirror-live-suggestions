# ProseMirror Live Suggestions

A `ProseMirror` (https://prosemirror.net/) plugin that aims to support *live suggestions* (sometimes also refered to as *track changes* or *live tracking*). A user shall be able to suggest changes to a document in a live collaborative environment using `Yjs` (https://docs.yjs.dev/).

If you would like to join, contact us via Discord (https://discord.gg/5khngPCUvs) or join the discussion in the Yjs forum (https://discuss.yjs.dev/t/live-tracking-track-changes/293).
